### A Pluto.jl notebook ###
# v0.19.2

using Markdown
using InteractiveUtils

# ╔═╡ a497653c-759d-4efd-bd5e-1409f604be7e
using DataStructures

# ╔═╡ 2613d047-50cd-4df8-8093-a89d990630a1
using PlutoUI, InteractiveUtils

# ╔═╡ ac27215c-0755-4476-a4d0-42d110e6457a
mutable struct State
	name::String
	position::Int64
	parcel::Vector{Bool}
end

# ╔═╡ ea91dff0-d131-4316-9d4c-7545641ac165
@enum Actions me mw mu md co

# ╔═╡ 7c2b2059-e99b-4ada-9487-bcf84f5b9c41
struct Action
	name::String
	cost::Int64
end

# ╔═╡ 76d53e2b-fdc6-4b42-94b1-e7255937c3b7
Action3 = Action("move up", 1)

# ╔═╡ 3564f7ee-fa7d-4e6d-ac70-e0e505111788
Action4 = Action("move down", 1)

# ╔═╡ e997dfe9-2ff1-4ed1-8722-79ec02d23ee4
Action1 = Action("move east", 2)

# ╔═╡ 21cb260d-b59d-4e11-9f96-db42de239b25
Action2 = Action("move west", 2)

# ╔═╡ 4328dbfd-31bb-451d-8b10-ef1858a2754b
Action5 = Action("collect", 5)

# ╔═╡ c52184fd-d177-44e9-ab82-51a89222e1fb
State1 = State("State 1", 1, [true, false, true])

# ╔═╡ fc25ebf8-8361-49a1-b517-72cda5979ade
State2 = State("State 2", 2, [true, true, false])

# ╔═╡ 777f443c-3d9d-44d7-8493-eb1bdc399dc6
State3 = State("State 3", 3, [false, true, true])

# ╔═╡ b8ce2ab5-1d5a-40c1-b83d-9728b359632f
State4 = State("State 4", 4, [false, true, true])

# ╔═╡ 26f7623c-a21f-4b32-913f-5cfc9c2dec37
State5 = State("State 5", 5, [true, false, true])

# ╔═╡ 8b970018-574f-4e33-90ec-ec395da1a55f
State6 = State("State 6", 1, [false, true, true])

# ╔═╡ fc03b276-36d8-4817-90df-2c3c92376fe4
State7 = State("State 7", 2, [false, false, true])

# ╔═╡ 20cbbc93-9eb3-4d31-afd5-b9e6c91f61ae
State8 = State("State 8", 3, [false, false,true])

# ╔═╡ c5436743-47be-4160-b4ca-cb5e0e225523
State9 = State("State 9", 4, [false, false, true])

# ╔═╡ 1c53fd1e-e4b2-460c-a0bf-c01bac258425
State10 = State("State 10", 5, [false, false, true])

# ╔═╡ e87a49bc-2216-41ee-b89c-e8ba255e1446
TransitionModel = Dict()

# ╔═╡ 066977d9-86eb-4b81-a3a8-e0946d4f7b26
push!(TransitionModel, State1 => [(Action2, State2), (Action5, State6), (Action3, State1)])

# ╔═╡ 66b6f2d6-f854-467e-b999-c1a957894313
push!(TransitionModel, State1 => [(Action2, State3), (Action5, State7), (Action1, State2)])

# ╔═╡ 050b913f-5970-4bb9-a650-9a4e190e996c
push!(TransitionModel, State1 => [(Action4, State4), (Action5, State8), (Action1, State3)])

# ╔═╡ 44910fcb-a032-476f-a908-dbd3ac6902a8
push!(TransitionModel, State1 => [(Action2, State5), (Action5, State9), (Action3, State4)])

# ╔═╡ 12294b45-054a-4e38-b56c-121bf86f738f
push!(TransitionModel, State9 => [(Action2, State5), (Action5, State10), (Action4, State10)])

# ╔═╡ 64ce16d5-bba9-4a48-8e32-f5977e464ef4
TransitionModel

# ╔═╡ 87a319de-5e0c-451b-b4df-df683dd24058
function heuristic(currentState, goalState)
	h = distance(currentState[1] - goalState[1]) + distance(cell[2] - goalState[2])
    return h
end

# ╔═╡ 0af488f1-a008-4dd3-9df3-bde37640c698
function astarS(TransitionModel,initialState, goalState)
	
    result = []
	frontier = Queue{State}()
	explored = []
	parents = Dict{State, State}()
	first_state = true
    enqueue!(frontier, initialState)
	parent = initialState
	
    while true
		if isempty(frontier)
			return []
		else
			currentState = dequeue!(frontier)
			push!(explored, currentState)
			candidates = TransitionModel[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(parents, single_candidate[2] => currentState)
					if (single_candiadte[2] in goal_state)
						return get_result(TransitionModel, parents,initialState, single_candidate[2])
					else
						enqueue!(frontier, single_candidate[2])
					end
				end
			end
		end
	end
end

# ╔═╡ fc2f5b8f-10bf-48ae-a8e8-f7edb313b905
function result(TransitionModel, ancestors, initialState, goalState)
	result = []
	explorer = goalState
	while !(explorer == initialState)
		current_state_ancestor = ancestors[explorer]
		related_transitions = TransitionModel[current_state_ancestor]
		for single_trans in related_transitions
			if single_trans[2] == explorer
				push!(result, single_trans[1])
				break
			else
				continue
			end
		end
		explorer = current_state_ancestor
	end
	return result
end

# ╔═╡ ff53c9bf-17bd-42c4-8bd8-7a233f271683
function findSearch(initialState, transition_dict, is_goal,all_candidates,
add_candidate, remove_candidate)
	explored = []
	ancestors = Dict{State, State}()
	the_candidates = add_candidate(all_candidates, initialState, 0)
	parent = initialState
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			current_state = t1
			the_candidates = t2
			push!(explored, current_state)
			candidates = transition_dict[current_state]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(ancestors, single_candidate[2] => current_state)
					if (is_goal(single_candidate[2]))
						return result(transition_dict, ancestors,
initialState, single_candidate[2])
					else
						the_candidates = add_candidate(the_candidates,
single_candidate[2], single_candidate[1].cost)
						end
					end
				end
			end
		end
end

# ╔═╡ eedc90cb-e9cc-48aa-9aca-b87a718f75e7
function goalT(currentState::State)
    return ! (currentState.parcel[1] || currentState.parcel[2])
end

# ╔═╡ 1cff054e-939a-4017-9b7e-ad1a1dbf86f9
function addQueue(queue::Queue{State}, state::State, cost::Int64)
    enqueue!(queue, state)
    return queue
end

# ╔═╡ fbb6be1e-ad44-40ad-b595-67b622c79276
function addStack(stack::Stack{State}, state::State, cost::Int64)
    push!(stack, state)
    return stack
end

# ╔═╡ 16a644ff-ac62-4a0d-ac9b-0d0776fb5444
function removeQueue(queue::Queue{State})
    removed = dequeue!(queue)
    return (removed, queue)
end

# ╔═╡ b309598a-fffb-4175-b946-c5ee7f1e7713
function removeStack(stack::Stack{State})
    removed = pop!(stack)
    return (removed, stack)
end

# ╔═╡ 3111646a-0d86-4d08-8e80-97fc57907a64
findSearch(State1, TransitionModel, goalT, Queue{State}(), addQueue, 
removeQueue)

# ╔═╡ 4b00d688-9311-4852-9544-1d6a617b79df
function astar(rowStoreys,colOffices)
	rowStoreys, colOffices = 0
	companybuilding = [(i,j) 
		for i = 0:rowStoreys  
			for j = 0:colOffices]
	return companybuilding
end

# ╔═╡ aa0825dc-d344-4712-ab7a-3eb6a16cc197
function astar(2,3)

# ╔═╡ 9364bce3-f5d9-4a95-a4d3-ecc876bb95db
begin
	row = 5
	col = 4
	companybuilding = [(i,j) 
		for i = 0:row  
			for j = 0:col]
end

# ╔═╡ c15c1d8b-cdd4-4f30-8286-bc0ebba57b30
with_terminal() do
	println()
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═a497653c-759d-4efd-bd5e-1409f604be7e
# ╠═2613d047-50cd-4df8-8093-a89d990630a1
# ╠═ac27215c-0755-4476-a4d0-42d110e6457a
# ╠═ea91dff0-d131-4316-9d4c-7545641ac165
# ╠═7c2b2059-e99b-4ada-9487-bcf84f5b9c41
# ╠═76d53e2b-fdc6-4b42-94b1-e7255937c3b7
# ╠═3564f7ee-fa7d-4e6d-ac70-e0e505111788
# ╠═e997dfe9-2ff1-4ed1-8722-79ec02d23ee4
# ╠═21cb260d-b59d-4e11-9f96-db42de239b25
# ╠═4328dbfd-31bb-451d-8b10-ef1858a2754b
# ╠═c52184fd-d177-44e9-ab82-51a89222e1fb
# ╠═fc25ebf8-8361-49a1-b517-72cda5979ade
# ╠═777f443c-3d9d-44d7-8493-eb1bdc399dc6
# ╠═b8ce2ab5-1d5a-40c1-b83d-9728b359632f
# ╠═26f7623c-a21f-4b32-913f-5cfc9c2dec37
# ╠═8b970018-574f-4e33-90ec-ec395da1a55f
# ╠═fc03b276-36d8-4817-90df-2c3c92376fe4
# ╠═20cbbc93-9eb3-4d31-afd5-b9e6c91f61ae
# ╠═c5436743-47be-4160-b4ca-cb5e0e225523
# ╠═1c53fd1e-e4b2-460c-a0bf-c01bac258425
# ╠═e87a49bc-2216-41ee-b89c-e8ba255e1446
# ╠═066977d9-86eb-4b81-a3a8-e0946d4f7b26
# ╠═66b6f2d6-f854-467e-b999-c1a957894313
# ╠═050b913f-5970-4bb9-a650-9a4e190e996c
# ╠═44910fcb-a032-476f-a908-dbd3ac6902a8
# ╠═12294b45-054a-4e38-b56c-121bf86f738f
# ╠═64ce16d5-bba9-4a48-8e32-f5977e464ef4
# ╠═87a319de-5e0c-451b-b4df-df683dd24058
# ╠═0af488f1-a008-4dd3-9df3-bde37640c698
# ╠═fc2f5b8f-10bf-48ae-a8e8-f7edb313b905
# ╠═ff53c9bf-17bd-42c4-8bd8-7a233f271683
# ╠═eedc90cb-e9cc-48aa-9aca-b87a718f75e7
# ╠═1cff054e-939a-4017-9b7e-ad1a1dbf86f9
# ╠═fbb6be1e-ad44-40ad-b595-67b622c79276
# ╠═16a644ff-ac62-4a0d-ac9b-0d0776fb5444
# ╠═b309598a-fffb-4175-b946-c5ee7f1e7713
# ╠═3111646a-0d86-4d08-8e80-97fc57907a64
# ╠═4b00d688-9311-4852-9544-1d6a617b79df
# ╠═aa0825dc-d344-4712-ab7a-3eb6a16cc197
# ╠═9364bce3-f5d9-4a95-a4d3-ecc876bb95db
# ╠═c15c1d8b-cdd4-4f30-8286-bc0ebba57b30
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
